# GraphQLet

GraphQLet is a Java GraphQL client based on the HttpClient of Java 11, JSON-P and JSON-B. The purpose of this project is keeping simple for anyone who wants to invoke GraphQL services easily.

## Installation
1. Setup repository registry in `pom.xml`
```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/16753474/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/16753474/packages/maven</url>
    </repository>
    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/16753474/packages/maven</url>
    </snapshotRepository>
</distributionManagement>
```
2. Add dependency in your `pom.xml`
```xml
<dependency>
    <groupId>io.qstudio</groupId>
    <artifactId>graphqlet</artifactId>
    <version>0.2</version>
</dependency>
```
3. [JSON-P](https://eclipse-ee4j.github.io/jsonp/) and [JSON-B](http://json-b.net/) are just specs. So, please make sure there is an implementation in your runtime. If no, maybe you can consider add a dependency as follow.

```xml
<!-- for JSON-P -->
<dependency>
    <groupId>org.glassfish</groupId>
    <artifactId>javax.json</artifactId>
    <version>1.1.4</version>
</dependency>
<!-- for JSON-B -->
<dependency>
    <groupId>org.eclipse</groupId>
    <artifactId>yasson</artifactId>
    <version>1.0.6</version>
    <scope>test</scope>
</dependency>
```

## Sample Code
### STEP 1: Create GraphQLet
Initiate a GraphQLet by pass the service url.
```java
var serviceUrl = "{the url of an actual GraphQL service}";
var client = new GraphQLet(serviceUrl);
```
You can using custom HttpClient if needed.
```java
var client = new GraphQLet(
    serviceUrl,
    HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_1_1)
        .build()
);
```
### STEP 2: Invoke an API
```java
var query = "{the query definition written using GraphQL}";
// If your query need a varible create like this, otherwise, just use Optional.empty().
var variable = Optional.of(Json.createObjectBuilder()
    .add("variableName", "variableValue")
    .build()
);
// If there are multiple queries defined in your query, you need to specify which operation to be executed. Otherwise, using Optional.empty().
var operationName = Optional.of("operation");
var response = client.invoke(query, variable, operationName);
```
### STEP 3: Check the reponse status if needed
```java
// If you need to check the response status, just do like this.
if (response.statusCode() != 200) {
    // do something if not invoked successful
}
```
### STEP 4: Extract the result based on GraphQL API return value
[CASE 1] Extract an JsonValue object
```java
// The result is an Optional<JsonValue>, you can convert it to other type on demand. Please check the API document of JSON-P.
var result = client.extractResult(response);
```
[CASE 2] Extract an Specific Pojo, must be an Pojo type
```java
// The result is an Optional<YourPojo>
var result = client.extractResult(response, YourPojo.class);
```
[CASE 3] Extract an List of Specific Pojo.
```java
// The result is a List<YourPojo>
var result = client.extractResultList(response, YourPojo.class);
```
