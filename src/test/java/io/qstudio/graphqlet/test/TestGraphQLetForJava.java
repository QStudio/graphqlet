/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright © 2019-present QStudio and the project authors
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qstudio.graphqlet.test;

import io.qstudio.graphqlet.GraphQLet;

import org.junit.jupiter.api.Test;

import javax.json.Json;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * GraphQLet test cases for graphql-java.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.2
 * @since 0.2
 */
public class TestGraphQLetForJava {

    /**
     * Turn to false if push to GitLab.com
     */
    private static final boolean TEST_SERVER_ON = false;

    /**
     * Local test server.
     */
    private static final String TEST_API_URL = "http://localhost:8080/graphql";

    /**
     * Query for invoke local test API.
     */
    private static final String QUERY = "mutation putType($type: TemplateTypeInput!) {\n"
        + "    putTemplateType(type: $type) {\n"
        + "        id\n"
        + "        name\n"
        + "    }\n"
        + "}\n"
        + "\n"
        + "query types {\n"
        + "    templateTypes {\n"
        + "        id\n"
        + "        name\n"
        + "    }\n"
        + "}";

    /**
     * The shared instance of GraphQLet.
     */
    private static final GraphQLet CLIENT = new GraphQLet(TEST_API_URL);

    /**
     * Test for single object result.
     */
    @Test
    public void testSingleResult() {
        if (!TEST_SERVER_ON) {
            return;
        }
        try {
            var name = "case1";
            var variable = Json.createObjectBuilder()
                .add("type", Json.createObjectBuilder()
                    .add("name", name)
                    .build()
                ).build();
            var response = CLIENT.invoke(QUERY, Optional.of(variable), Optional.of("putType"));

            var result1_ = CLIENT.extractResult(response);
            assertTrue(result1_.isPresent());
            var result1 = result1_.get().asJsonObject();
            assertEquals(name, result1.getString("name"));
            assertNotNull(result1.getString("id"));

            var result2_ = CLIENT.extractResult(response, Type.class);
            assertTrue(result2_.isPresent());
            var result2 = result2_.get();
            assertEquals(name, result2.getName());
            assertNotNull(result2.getId());
        } catch (IOException | InterruptedException ex) {
            System.out.println(ex.getCause());
        }
    }

    /**
     * Test for list result.
     */
    @Test
    public void testListResult() {
        if (!TEST_SERVER_ON) {
            return;
        }
        try {
            var response = CLIENT.invoke(QUERY, Optional.empty(), Optional.of("types"));
            var result = CLIENT.extractResultList(response, Type.class);
            assertFalse(result.isEmpty());
        } catch (IOException | InterruptedException ex) {
            System.out.println(ex.getCause());
        }
    }

    /**
     * Sample class for extract result with type.
     */
    public static class Type {

        private String id;

        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
