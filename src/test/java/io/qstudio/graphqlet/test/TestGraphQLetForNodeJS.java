/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright © 2019-present QStudio and the project authors
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qstudio.graphqlet.test;

import io.qstudio.graphqlet.GraphQLet;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.http.HttpClient;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * GraphQLet test cases for Node.js + graphql-yoga.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.2
 * @since 0.2
 */
public class TestGraphQLetForNodeJS {

    /**
     * Turn to false if push to GitLab.com
     */
    private static final boolean TEST_SERVER_ON = false;

    /**
     * Local test server.
     */
    private static final String TEST_API_URL = "http://localhost:4000/";

    /**
     * The shared instance of GraphQLet.
     * Use http 1.1 instead.
     */
    private static final GraphQLet CLIENT = new GraphQLet(
        TEST_API_URL,
        HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .build()
    );

    /**
     * Test the connection by a simple request. 
     */
    @Test
    public void testConnection() {
        if (!TEST_SERVER_ON) {
            return;
        }
        try {
            var response = CLIENT.invoke("{info}", Optional.empty(), Optional.empty());
            assertEquals(200, response.statusCode());
            var result1_ = CLIENT.extractResult(response, String.class);
            assertTrue(result1_.isPresent());
            var result = result1_.get();
            assertEquals("Hello GraphQLet", result);
        } catch (IOException | InterruptedException ex) {
            System.out.println(ex.getCause());
        }
    }
}
