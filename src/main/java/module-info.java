/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright © 2019-present QStudio and the project authors
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Defines the GraphQL Client which is called GraphQLet.
 * 
 * @author Arren Ping at QStudio.io
 * @version 0.2
 * @since 0.1
 * @since 0.2 add require for java.json.bind
 */
module qstudio.graphqlet {
    requires java.net.http;
    requires java.json;
    requires java.json.bind;
    exports io.qstudio.graphqlet;
}
