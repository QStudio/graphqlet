/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright © 2020-present QStudio and the project authors
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.qstudio.graphqlet;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A GraphQL client based on the HttpClient of Java 11, JSON-P and JSON-B.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.2
 * @since 0.1
 */
public class GraphQLet {

    /**
     * The response data field name.
     *
     * @since 0.1.1
     */
    private final static String DATA = "data";

    /**
     * The client using to invoke GraphQL API.
     */
    private HttpClient client;

    /**
     * The GraphQL service url.
     */
    private final String serviceUrl;

    /**
     * The Jsonb using to convert JSON string to object.
     *
     * @since 0.2
     */
    private final Jsonb jsonb = JsonbBuilder.create();

    /**
     * Create a {@code GraphQLet}.
     *
     * @param serviceUrl the GraphQL service url.
     */
    public GraphQLet(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    /**
     * Create a {@code GraphQLet} with a specific {@code client}.
     * If client is null, default client will be used.
     *
     * @param serviceUrl the GraphQL service url.
     * @param client the client to be used.
     * @since 0.2
     */
    public GraphQLet(String serviceUrl, HttpClient client) {
        this.serviceUrl = serviceUrl;
        this.client = client;
    }

    /**
     * Invoke a GraphQL API and get the response.
     *
     * @param query the GraphQL query definition string.
     * @param variable the variable using in the {@code query}.
     * @param operationName the operation name defined in the {@code query}.
     * @return the {@code HttpResponse} of the API
     * @throws IOException throwed by {@code HttpClient}
     * @throws InterruptedException throwed by {@code HttpClient}
     * @see HttpClient#send(java.net.http.HttpRequest, java.net.http.HttpResponse.BodyHandler)
     */
    public HttpResponse<String> invoke(String query, Optional<JsonObject> variable, Optional<String> operationName) throws IOException, InterruptedException {
        var payload = toPayload(query, variable, operationName);
        return invoke(payload);
    }

    /**
     * Extract the result of an GraphQL API {@code HttpResponse}.
     *
     * @param response an GraphQL API response
     * @return the result data of {@code response} if empty means can not be converted
     * @since 0.1
     * @since 0.1.1 the return value changed to Optional
     */
    public Optional<JsonValue> extractResult(HttpResponse<String> response) {
        JsonValue result = null;
        try (var reader = Json.createReader(new StringReader(response.body()))) {
            var json = reader.readObject();
            if (json.containsKey(DATA)) {
                result = getData(json);
            }
        } catch (RuntimeException ex) {
        }
        return Optional.ofNullable(result);
    }

    /**
     * Extract the result of an GraphQL API {@code HttpResponse}.
     *
     * @param <T> the type of result
     * @param response an GraphQL API response
     * @param type the class of result type
     * @return the result data of {@code response} if empty means can not be converted
     * @since 0.2
     */
    public <T> Optional<T> extractResult(HttpResponse<String> response, Class<T> type) {
        T result = null;
        try (var reader = Json.createReader(new StringReader(response.body()))) {
            var json = reader.readObject();
            if (json.containsKey(DATA)) {
                result = jsonb.fromJson(getData(json).toString(), type);
            }
        } catch (RuntimeException ex) {
        }
        return Optional.ofNullable(result);
    }

    /**
     * Extract the result list of an GraphQL API {@code HttpResponse}.
     *
     * @param <T> the type of result
     * @param response an GraphQL API response
     * @param type the class of result type
     * @return the result data of {@code response} if empty means can not be converted
     * @since 0.2
     */
    public <T> List<T> extractResultList(HttpResponse<String> response, Class<T> type) {
        var result = new ArrayList<T>();
        try (var reader = Json.createReader(new StringReader(response.body()))) {
            var json = reader.readObject();
            if (json.containsKey(DATA)) {
                getData(json).asJsonArray().stream()
                    .map(e -> jsonb.fromJson(e.toString(), type))
                    .forEach(result::add);
            }
        } catch (RuntimeException ex) {
        }
        return result;
    }

    /**
     * Generate the payload needed by invoking a GraphQL API.
     *
     * @param query the GraphQL query definition string.
     * @param variable the variable using in the {@code query}.
     * @param operationName the operation name defined in the {@code query}
     * @return the payload string
     */
    private String toPayload(String query, Optional<JsonObject> variables, Optional<String> operationName) {
        var builder = Json.createObjectBuilder();
        builder.add("query", query);
        variables.ifPresent(v -> builder.add("variables", v));
        operationName.ifPresent(o -> builder.add("operationName", o));
        return builder.build().toString();
    }

    /**
     * Invoke the GraphQL API by send an HTTP POST.
     *
     * @param payload the payload to POST
     * @return the {@code HttpResponse} of the API
     * @throws IOException
     * @throws InterruptedException
     */
    private HttpResponse<String> invoke(String payload) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
            .uri(URI.create(serviceUrl))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(payload))
            .build();
        return getClient().send(request, HttpResponse.BodyHandlers.ofString());
    }

    /**
     * Get client.
     *
     * @return the client to be used.
     * @since 0.2
     */
    private HttpClient getClient() {
        return client == null ? client = HttpClient.newHttpClient() : client;
    }

    /**
     * Get the data field value of a JsonObject.
     *
     * @param json the result of GraphQL API
     * @return the value of data field
     * @since 0.2
     */
    private JsonValue getData(JsonObject json) {
        return json.getJsonObject(DATA).values().iterator().next();
    }
}
